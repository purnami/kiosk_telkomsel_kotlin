package com.example.kiostelkomselkotlin.ui

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.kiostelkomselkotlin.databinding.ActivityMainBinding
import com.example.kiostelkomselkotlin.ui.login.LoginActivity
import com.example.kiostelkomselkotlin.ui.order.OrderActivity

class MainActivity : AppCompatActivity() {
    var binding: ActivityMainBinding?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding!!.root)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), 100)
        }

        binding!!.loginBtn.setOnClickListener {
            val intent = Intent(this@MainActivity, OrderActivity::class.java)
            startActivity(intent)
        }

        binding!!.regisBtn.setOnClickListener {

        }

        binding!!.buyBtn.setOnClickListener {

        }
    }
}