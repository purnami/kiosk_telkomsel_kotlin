package com.example.kiostelkomselkotlin.ui.login

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.kiostelkomselkotlin.databinding.ActivityLoginBinding
import com.example.kiostelkomselkotlin.ui.dialog.FailedLoginDialog
import com.example.kiostelkomselkotlin.ui.faceRecog.FaceRecogActivity
import com.example.kiostelkomselkotlin.ui.order.OrderActivity
import com.google.android.material.textfield.TextInputEditText
import java.io.ByteArrayOutputStream

class LoginActivity : AppCompatActivity() {
    var binding: ActivityLoginBinding?=null
    var etUsername : TextInputEditText?=null
    var etPassword :TextInputEditText?=null

    var failedLoginDialog : FailedLoginDialog?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding!!.root)

        etUsername = binding!!.loginUsername
        etPassword = binding!!.loginPassword

        failedLoginDialog=FailedLoginDialog(this)

        binding!!.loginBtn.setOnClickListener(clickLoginBtn)
    }

    val clickLoginBtn=View.OnClickListener {
        var error = false
        val login_username: String = etUsername!!.getText().toString()
        val login_password: String = etPassword!!.getText().toString()

        if (login_username == "") {
            error = true
            etUsername!!.error = "Username tidak boleh kosong !"
        } else if (login_password == "") {
            error = true
            etPassword!!.error = "Password tidak boleh kosong !"
        }

        if (!error){
            if (login_username.equals("abcd") && login_password.equals("1234")){
//                val intent = Intent(this@LoginActivity, FaceRecogActivity::class.java)
//                startActivity(intent)

                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                startActivityForResult(intent, 100)

            }else{
                failedLoginDialog!!.startFailedLoginDialog()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == 0) {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        } else {
            val bitmap = data!!.extras!!["data"] as Bitmap?
            val stream = ByteArrayOutputStream()
            bitmap!!.compress(Bitmap.CompressFormat.PNG, 100, stream)
            val byteArray = stream.toByteArray()
            val intent = Intent(this, OrderActivity::class.java)
            startActivity(intent)
        }
    }
}