package com.example.kiostelkomselkotlin.ui.order

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.kiostelkomselkotlin.R
import com.example.kiostelkomselkotlin.databinding.ActivityOrderBinding
import com.example.kiostelkomselkotlin.ui.MainActivity
import com.example.kiostelkomselkotlin.ui.order.changeCard.ChangeCardDialog
import com.example.kiostelkomselkotlin.ui.order.haloCardPayment.HaloCardDialog

class OrderActivity : AppCompatActivity() {
    var binding: ActivityOrderBinding?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order)

        binding = ActivityOrderBinding.inflate(layoutInflater)
        setContentView(binding!!.root)

        binding!!.logout.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        binding!!.changeCard.setOnClickListener{
            val changeCardDialog = ChangeCardDialog()
            changeCardDialog.show(supportFragmentManager, "example")
        }

        binding!!.halocardPayment.setOnClickListener {
            val haloCardDialog=HaloCardDialog()
            haloCardDialog.show(supportFragmentManager, "example")
        }
    }
}