package com.example.kiostelkomselkotlin.ui.order.haloCardPayment

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatDialogFragment
import com.example.kiostelkomselkotlin.R
import com.example.kiostelkomselkotlin.ui.order.OrderActivity
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class HaloCardDialog : AppCompatDialogFragment() {
    private var dialog: AlertDialog? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        val inflater = requireActivity().layoutInflater
        val inflate_view: View = inflater.inflate(R.layout.custom_dialog, null)

        val ok = inflate_view.findViewById<Button>(R.id.oke)
        val cancel = inflate_view.findViewById<Button>(R.id.cancel)
        val progress = inflate_view.findViewById<ProgressBar>(R.id.progress)
        val text = inflate_view.findViewById<TextView>(R.id.textView)
        val textInput1=inflate_view.findViewById<TextInputLayout>(R.id.text_input_1)
        val textInput2=inflate_view.findViewById<TextInputLayout>(R.id.text_input_2)
        val textInput3=inflate_view.findViewById<TextInputLayout>(R.id.text_input_3)
        val editText2=inflate_view.findViewById<TextInputEditText>(R.id.edit_text_2)
        val editText3=inflate_view.findViewById<TextInputEditText>(R.id.edit_text_3)

        builder.setView(inflate_view)
        builder.setCancelable(true)

        text.visibility = View.GONE
        textInput1.visibility= View.GONE

        var haloNumber = editText2.text
        var waNumber = editText3.text
        ok.setOnClickListener {
            val intent = Intent(activity, OrderActivity::class.java)
            requireActivity().startActivity(intent)
        }
        cancel.setOnClickListener {
            val intent = Intent(activity, OrderActivity::class.java)
            requireActivity().startActivity(intent)
        }

        dialog = builder.create()
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.setCancelable(false)
        return dialog!!
    }
}