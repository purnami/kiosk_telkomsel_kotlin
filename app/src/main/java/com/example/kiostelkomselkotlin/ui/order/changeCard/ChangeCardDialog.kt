package com.example.kiostelkomselkotlin.ui.order.changeCard

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatDialogFragment
import com.example.kiostelkomselkotlin.R
import com.example.kiostelkomselkotlin.ui.order.OrderActivity
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class ChangeCardDialog : AppCompatDialogFragment() {
    private var dialog: AlertDialog? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        val inflater = requireActivity().layoutInflater
        val inflate_view: View = inflater.inflate(R.layout.custom_dialog, null)

        val ok = inflate_view.findViewById<Button>(R.id.oke)
        val cancel = inflate_view.findViewById<Button>(R.id.cancel)
        val progress = inflate_view.findViewById<ProgressBar>(R.id.progress)
        val text = inflate_view.findViewById<TextView>(R.id.textView)
        val textInput1=inflate_view.findViewById<TextInputLayout>(R.id.text_input_1)
        val textInput2=inflate_view.findViewById<TextInputLayout>(R.id.text_input_2)
        val textInput3=inflate_view.findViewById<TextInputLayout>(R.id.text_input_3)
        val editText1=inflate_view.findViewById<TextInputEditText>(R.id.edit_text_1)

        builder.setView(inflate_view)
        builder.setCancelable(true)

        text.visibility = View.GONE
        textInput2.visibility=View.GONE
        textInput3.visibility=View.GONE

        var oldNumber = editText1.text
        ok.setOnClickListener {
//            Log.d("oldnumber", ""+oldNumber)
            Log.d("last", ""+ oldNumber!!.endsWith("9"))
            if(oldNumber!!.endsWith("9")){
                text.visibility=View.VISIBLE
                textInput1.visibility=View.GONE
                text.text= "Nomor ini masih aktif dalam 4 jam terakhir"
                cancel.visibility=View.GONE
                ok.setOnClickListener {
                    val intent = Intent(activity, OrderActivity::class.java)
                    requireActivity().startActivity(intent)
                }
            }else{
                text.visibility=View.VISIBLE
                textInput1.visibility=View.GONE
                text.text="Term & Condition\n" +
                        "\n" +
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempus ullamcorper dui sed placerat. Mauris venenatis nulla ac ultricies iaculis. Morbi sollicitudin blandit eleifend. Cras lacus sapien, scelerisque a risus scelerisque, tempus gravida odio. Etiam tincidunt egestas mauris, in cursus nulla blandit in. Nunc bibendum at tortor a aliquam. Vivamus lectus nibh, accumsan et posuere vitae, ultrices vitae velit. Curabitur maximus nulla ut nunc imperdiet tincidunt. Donec bibendum facilisis ligula, nec congue ante tincidunt nec. Vivamus efficitur urna magna, sit amet maximus purus fermentum id. Duis rutrum pellentesque porttitor. Donec aliquet, leo a cursus tempus, ex sapien efficitur mauris, a placerat nunc diam vel neque. Sed tincidunt, leo luctus hendrerit egestas, metus eros scelerisque quam, vitae elementum lacus dolor vel ipsum."
                ok.setOnClickListener {
                    val intent = Intent(activity, OrderActivity::class.java)
                    requireActivity().startActivity(intent)
                }
            }
        }
        cancel.setOnClickListener {
            val intent = Intent(activity, OrderActivity::class.java)
            requireActivity().startActivity(intent)
        }

        dialog = builder.create()
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.setCancelable(false)
        return dialog!!
    }

}

