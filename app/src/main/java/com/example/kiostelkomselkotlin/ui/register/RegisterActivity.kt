package com.example.kiostelkomselkotlin.ui.register

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.kiostelkomselkotlin.R

class RegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
    }
}