package com.example.kiostelkomselkotlin.ui.dialog

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.view.View
import android.widget.TextView
import com.example.kiostelkomselkotlin.R
import com.example.kiostelkomselkotlin.ui.login.LoginActivity

class FailedLoginDialog {
    private var activity: Activity? = null
    private var dialog: AlertDialog? = null

    constructor(activity: Activity){
        this.activity=activity
    }

    fun startFailedLoginDialog() {
        val builder = AlertDialog.Builder(activity)
        val inflater = activity!!.layoutInflater
        val inflate_view: View = inflater.inflate(R.layout.failed_login_dialog, null)
        val ok = inflate_view.findViewById<TextView>(R.id.oke)
        builder.setView(inflate_view)
        builder.setCancelable(true)
        ok.setOnClickListener {
            val intent = Intent(activity, LoginActivity::class.java)
//            intent.putExtra(LoginActivity.EXTRA_SCREEN, screen)
            activity!!.startActivity(intent)
            //                activity.startActivity(new Intent(activity, LoginActivity.class));
//                Intent intent=new Intent(context, LoginActivity.class);
//                activity.startActivity(intent);
        }
        dialog = builder.create()
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.setCancelable(false)
        dialog!!.show()
    }

    fun dismissDialog() {
        dialog!!.dismiss()
    }
}